from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import Context, loader
from tour.form import ContactForm
from django.template import RequestContext
from django.http import HttpResponseRedirect 
from django.core.urlresolvers import reverse
from tour.models import Contact
from django.core.context_processors import csrf
#from django.template.context_processors.csrf import 
#from django.views.decorators.csrf import csrf_protect
#from django.views.decorators.cache import cache_page
#from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):

    # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    #context_dict = {'boldmessage': "I am bold font from the context"}

    # Return a rendered response to send to the client.
    # We make use of the shortcut function to make our lives easier.
    # Note that the first parameter is the template we wish to use.

    return render(request, 'tour/camp_perrin.html')
	
def bar(request):
	
	return render(request, 'tour/bar.html')

def bananier(request):
	
	return render(request, 'tour/bananier.html')
	
def activites(request):
	
	return render(request, 'tour/activites.html')	

def arbrepin(request):
	
	return render(request, 'tour/arbrepin.html')	

def belle(request):
	
	return render(request, 'tour/belle.html')

def cocotier(request):
	
	return render(request, 'tour/cocotier.html')

def eglise(request):
	
	return render(request, 'tour/eglise.html')	
	

def fruits(request):
	
	return render(request, 'tour/fruits.html')	
	
def entete(request):
	
	return render(request, 'tour/entete.html')	
	
def fruitslide(request):
	
	return render(request, 'tour/fruitslide.html')
	
	
def gallerie(request):
	
	return render(request, 'tour/gallerie.html')
	
def grotte(request):
	
	return render(request, 'tour/grotte.html')	
	
def hotel(request):
	
	return render(request, 'tour/hotel.html')

def lerecul(request):
	
	return render(request, 'tour/lerecul.html')	
	
def mais(request):
	
	return render(request, 'tour/mais.html')	

def maison(request):
	
	return render(request, 'tour/maison.html')	

def maison1(request):
	
	return render(request, 'tour/maison1.html')	

def manguier(request):
	
	return render(request, 'tour/manguier.html')

def panier(request):
	
	return render(request, 'tour/panier.html')		

def paysage(request):
	
	return render(request, 'tour/paysage.html')	
	
def place(request):
	
	return render(request, 'tour/place.html')	

	
def routesaut(request):
	
	return render(request, 'tour/routesaut.html')

def saut(request):
	
	return render(request, 'tour/saut.html')	

def sautMathurine(request):
	
	return render(request, 'tour/sautMathurine.html')	

def slide(request):
	
	return render(request, 'tour/slide.html')

def tibi(request):
	
	return render(request, 'tour/tibi.html')

def touriste(request):
	
	return render(request, 'tour/touriste.html')

def unecour(request):
	
	return render(request, 'tour/unecour.html')
	
def unecour1(request):
	
	return render(request, 'tour/unecour1.html')	

def unerue(request):
	
	return render(request, 'tour/unerue.html')

def visite(request):
	
	return render(request, 'tour/visite.html')

def sitestouristiques(request):
    enr = Contact.objects.all()
	   # Handle file upload
    if request.method == 'GET':
        form = ContactForm(request.GET)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('tour.views.sitestouristiques'))
        #else:
			 #print(form.errors)
    else:
        form = ContactForm()  # A empty, unbound form

    return render_to_response( 'tour/sitestouristiques.html',locals())
	#return render_to_response('tour/sitestouristiques.html', {'form':form}, context_instance=RequestContext(request))
	
	#return render(request, 'tour/sitestouristiques.html')

def val1(request):
	
	return render(request, 'tour/val1.html')	
	
def val2(request):
	
	return render(request, 'tour/val2.html')

def val3(request):
	
	return render(request, 'tour/val3.html')	
	
